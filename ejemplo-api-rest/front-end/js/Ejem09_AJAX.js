/* Haremos una petición al servidor mediante la función fetch() de HTML5, y capturaremos los datos JSON en una función callback, los convertiremos a un objeto JavaScript y rellenaremos la página con esos datos al estilo React / Angular: 
Es decir, vamos a hacer un componente web.*/
// Template string de JS 2015 ó EcmaScript 6

let divComp = document.getElementById("componente-articulos-ajax");
// fetch() hace la llamada AJAX, que devuelve un obj tipo Promise
// Una promesa es un objeto que contendrá un recurso dado.
let promesaJSON = fetch("/articulos.json");
// La siguiente función será llamada cuando la promesa reciba el dato del servidor. Este dato se pasa parámetro "respuestaJSON"
promesaJSON.then(function(respuestaJSON) {
    return respuestaJSON.json(); // También devuelve una promesa
}).then(function(arrayArticulos) { // Array de obj
    divComp.innerHTML = "";
    for (articulo of arrayArticulos) { // Recorremos el array, donde articulo es cada objeto JS
        // Mediante las templates string de ES6, rellenamos con los datos
        let articleHTML = `
        <article class="articulo borde-suave-azul">
            <h1>${articulo.titulo}</h1>
            <h2 class="titulo-articulo">${articulo.subtitulo}</h2>
            <p>${articulo.cuerpo}</p>
        </article>`;
        divComp.innerHTML += articleHTML; // Al contenido del DIV añadimos los artículos
        inicializarArticulosDOM();
    }
});