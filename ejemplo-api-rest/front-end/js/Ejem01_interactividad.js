let articulos_dom = 200;
articulos_dom = document.getElementsByTagName("article");

function funCuandoHaceClick(evt) {
    for (let i = 0; i < articulos_dom.length; i++) {
        let art_dom_2 = articulos_dom[i];
        art_dom_2.className = "articulo";
    }
    this.className = "articulo articulo-seleccionado";
    // this.innerHTML += "Añadido al aritculo " + evt.x;
};

function inicializarArticulosDOM() {
    articulos_dom = document.getElementsByTagName("article");
    for (var i = 0; i < articulos_dom.length; i++) {
        var art_dom = articulos_dom[i];
        art_dom.onclick = funCuandoHaceClick;
    }
}